//
//  main.c
//  bitset
//
//  Created by kyung kim on 3/23/13.
//  Copyright (c) 2013 kyung kim. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>



unsigned int bitIndex[] = {
    0x80000000,
    0x40000000,
    0x20000000,
    0x10000000,
    0x08000000,
    0x04000000,
    0x02000000,
    0x01000000,
    0x00800000,
    0x00400000,
    0x00200000,
    0x00100000,
    0x00080000,
    0x00040000,
    0x00020000,
    0x00010000,
    0x00008000,
    0x00004000,
    0x00002000,
    0x00001000,
    0x00000800,
    0x00000400,
    0x00000200,
    0x00000100,
    0x00000080,
    0x00000040,
    0x00000020,
    0x00000010,
    0x00000008,
    0x00000004,
    0x00000002,
    0x00000001
};

void bit_set(unsigned data[],int index) {
    int intOffset = index / 32;
    int bitOffset = index % 32;

    data[intOffset] |= bitIndex[bitOffset];
}


void bit_clear(unsigned data[],int index) {
    int intOffset = index / 32;
    int bitOffset = index % 32;
    
    data[intOffset] &= (~bitIndex[bitOffset]);
}

int main(int argc, const char * argv[])
{
    int size = 100;
    int intSize = (size / (sizeof(int) * 8)) + 1;
    unsigned *data = (unsigned*)malloc(sizeof(int)*intSize);
    
    memset(data,0,intSize*sizeof(unsigned));
    
    bit_set(data, 50);
    printf("%d\n",data[1]);
    bit_clear(data, 50);
    printf("%d\n",data[1]);
    
    return 0;
}

