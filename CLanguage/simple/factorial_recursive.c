
#include <stdio.h>
#include <stdlib.h>


long fact_recursive(int n, long base) {

    if (n==1)
        return base;

    return fact_recursive(n-1,n*base);
}

int main(int argc,char *argv[])
{
    int n;
    if(argc<2) {
        printf("%s n",argv[0]);
        exit(-1);
    }
    n=atoi(argv[1]);

    printf ("%ld\n",fact_recursive(n,1));
}
