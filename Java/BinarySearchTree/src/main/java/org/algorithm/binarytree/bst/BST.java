/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarytree.bst;

import org.algorithm.binarytree.BTJob;
import org.algorithm.binarytree.Node;
import org.apache.log4j.Logger;

/**
 *
 * @author blueskywalker
 */
public class BST<E extends Comparable> {

    static protected Logger logger = Logger.getLogger(BST.class);
    protected BSNode<E> root;

    public boolean add(E value) {
        if (root == null) {
            root = new BSNode<E>(value);
            return true;
        }
        return root.add(new BSNode<E>(value))!=null;
    }

    public E saerch(E value) {
        Node<E> node = search(root,value);
        if(node!=null) {
            return node.getValue();
        }
        return null;
    }
    
    public Node<E> search(Node<E>node,E value) {
        
        if(node==null)
            return null;
        
        int cmp = node.getValue().compareTo(value);
        if (cmp < 0) {
            return search(node.getRight(),value);
        } else if (cmp > 0) {
            return search(node.getLeft(),value);
        } 
        return node;
    }

    public void inOrderJob(BTJob job) {
        root.inOrderJob(job);
    }
    
    public void preOrderJob(BTJob job) {
        root.preOrderJob(job);
    }
    
    public void postOrderJob(BTJob job) {
        root.postOrderJob(job);
    }
}
