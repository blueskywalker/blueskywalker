/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarytree.redblack;

import org.algorithm.binarytree.bst.BSNode;

/**
 *
 * @author blueskywalker
 */
public class RBNode<E extends Comparable> extends BSNode<E> {
    public static enum Color { RED,BLACK };
    private Color colour;
    RBNode<E> parent;

    
    public RBNode(E value) {
        super(value);
        this.colour = Color.RED;
    }

    @Override
    public RBNode<E> getLeft() {
        return (RBNode<E>) super.getLeft(); 
    }

    @Override
    public RBNode<E> getRight() {
        return (RBNode<E>) super.getRight(); 
    }

    @Override
    public RBNode<E> add(BSNode<E> node) {
         RBNode<E> ancestor = (RBNode<E>) super.add(node);
         
         if(ancestor!=null) {
             ((RBNode)node).setParent(ancestor);
         }
         return ancestor;
    }

    public RBNode<E> getParent() {
        return parent;
    }

    public void setParent(RBNode<E> parent) {
        this.parent = parent;
    }

    public Color getColour() {
        return colour;
    }

    public void setColour(Color colour) {
        this.colour = colour;
    }


}

