package org.algorithm.binarytree.bst;

import org.algorithm.binarytree.BTJob;

/**
 * Hello world!
 *
 */
public class BSTMain 
{
    public static void main( String[] args )
    {
        int [] data = new int[] { 10,23,34,3,5,34,17,29,24,11,29 };
        BST<Integer> bst = new BST<Integer>();
        
        for (int item: data) {
            bst.add(new Integer(item));
        }
        
        bst.inOrderJob(new BTJob<Integer>() {
            public void doIt(Integer value) {
                System.out.println(value);
            }
        });
        
    }
}
