/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarytree.bst;

import org.algorithm.binarytree.Node;

/**
 *
 * @author blueskywalker
 */
public class BSNode<E extends Comparable> extends Node<E> {

    public BSNode(E value) {
        super(value);
    }

    
    @Override
    public BSNode<E> getLeft() {
        return (BSNode) super.getLeft(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BSNode<E> getRight() {
        return (BSNode) super.getRight(); //To change body of generated methods, choose Tools | Templates.
    }
    
       
    public BSNode<E> add(BSNode<E> node) {
        int cmp = getValue().compareTo(node.getValue());
        if (cmp < 0) {
            if (getRight() == null) {
                setRight(node);
                return this;
            }
            return getRight().add(node);
        } else if (cmp > 0) {
            if (getLeft()== null) {
                setLeft(node);
                return this;
            }
            return getLeft().add(node);
        } else {
            logger.warn(String.format("Duplicated Key -[%s]", node.getValue().toString()));
        }
        return null;
    }
}
