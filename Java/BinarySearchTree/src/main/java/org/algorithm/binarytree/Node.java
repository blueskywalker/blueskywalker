/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarytree;

import org.apache.log4j.Logger;

/**
 *
 * @author blueskywalker
 */
public class Node<E extends Comparable> {

    static protected Logger logger = Logger.getLogger(Node.class);
    private E value;
    private Node<E> left;
    private Node<E> right;

    public Node() {
        value = null;
        left = null;
        right = null;
    }

    public Node(E value) {
        this.value = value;
        left = null;
        right = null;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public Node<E> getLeft() {
        return left;
    }

    public void setLeft(Node<E> left) {
        this.left = left;
    }

    public Node<E> getRight() {
        return right;
    }

    public void setRight(Node<E> right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "Node{" + "value=" + value + '}';
    }

    public void inOrderJob(BTJob job) {
        if (left != null) {
            left.inOrderJob(job);
        }

        job.doIt(value);

        if (right != null) {
            right.inOrderJob(job);
        }
    }

    public void preOrderJob(BTJob job) {
        job.doIt(value);

        if (left != null) {
            left.inOrderJob(job);
        }

        if (right != null) {
            right.inOrderJob(job);
        }
    }

    public void postOrderJob(BTJob job) {

        if (left != null) {
            left.inOrderJob(job);
        }

        if (right != null) {
            right.inOrderJob(job);
        }

        job.doIt(value);

    }
    
    public Node copy(Node node) {
        
        if(node==null)
            return null;
        
        Node ret = new Node();        
        ret.setValue(node.getValue());        
        ret.setLeft(copy(node.getLeft()));
        ret.setRight(copy(node.getRight()));
        
        return ret;        
    }
}
