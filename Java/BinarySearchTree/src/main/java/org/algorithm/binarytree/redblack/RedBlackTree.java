/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarytree.redblack;

import org.algorithm.binarytree.bst.BST;

/**
 *
 * @author blueskywalker * In addition to the requirements imposed on a binary
 * search trees, with red–black trees:[4] 1.A node is either red or black. 2.The
 * root is black. (This rule is sometimes omitted. Since the root can always be
 * changed from red to black, but not necessarily vice-versa, this rule has
 * little effect on analysis.) 3.All leaves (NIL) are black. (All leaves are
 * same color as the root.) 4.Both children of every red node are black. 5.
 * Every simple path from a given node to any of its descendant leaves contains
 * the same number of black nodes.
 */
public class RedBlackTree<E extends Comparable> extends BST<E> {

    public static final boolean VERIFY_RBTREE = true;

    public void verifyProperties() {
        if (VERIFY_RBTREE) {
            verifyProperty1((RBNode<E>) root);
            verifyProperty2();
            // Property 3 is implicit
            verifyProperty4((RBNode<E>) root);
            verifyProperty5();
        }
    }

    private void verifyProperty1(RBNode<E> node) {
        if(node==null)
            return;
        assert node.getColour() == RBNode.Color.BLACK || node.getColour() == RBNode.Color.RED;
        
        verifyProperty1(node.getLeft());
        verifyProperty1(node.getRight());
    }
    
    private void verifyProperty2() {
        if(root==null)
            return;
        
        assert ((RBNode)root).getColour() == RBNode.Color.BLACK;
    }
 
    private static RBNode.Color nodeColor(RBNode n) {
        return n == null ? RBNode.Color.BLACK : n.getColour()                                   ;
    }   
    
    private void verifyProperty4(RBNode<E> node) {
        if (node==null)
            return;
        
        if(nodeColor(node) == RBNode.Color.RED) {
            assert nodeColor(node.getLeft()) == RBNode.Color.BLACK;
            assert nodeColor(node.getRight()) == RBNode.Color.BLACK;
            assert nodeColor(node.getParent()) == RBNode.Color.BLACK;
        }
        
        verifyProperty4(node.getLeft());
        verifyProperty4(node.getRight());
    }
    
    private void verifyProperty5() {
        verifyProperty5Helper((RBNode<E>) root, 0, -1);
    }
    private int verifyProperty5Helper(RBNode<E> node,int blackCount,int pathBlackCount) {
        
        if(nodeColor(node)==RBNode.Color.BLACK) {
            blackCount++;
        }
        
        if(node == null) {
            if(pathBlackCount==-1) {
                pathBlackCount = blackCount;
            } else {
                assert blackCount == pathBlackCount;
            }
            return pathBlackCount;
        }
        
        pathBlackCount = verifyProperty5Helper(node.getLeft(), blackCount, pathBlackCount);
        pathBlackCount = verifyProperty5Helper(node.getRight(), blackCount, pathBlackCount);
        return pathBlackCount;
    }
    public RBNode<E> parent(RBNode<E> node) {
        if (node == null) {
            return null;
        }
        return node.getParent();
    }

    public RBNode<E> grandParent(RBNode<E> node) {
        if (node == null || node.getParent() == null) {
            return null;
        }

        return node.getParent().getParent();
    }

    public RBNode<E> sibling(RBNode<E> node) {
        if (node == node.getParent().getLeft()) {
            return node.getParent().getRight();
        } else {
            return node.getParent().getLeft();
        }
    }

    public RBNode<E> uncle(RBNode<E> node) {
        if (node == null || node.getParent() == null) {
            return null;
        }
        return sibling(node.getParent());
    }

    private void replaceNode(RBNode<E> nodeX, RBNode<E> nodeY) {

        if (nodeX.getParent() == null) {
            root = nodeY;
        } else {
            if (nodeX == parent(nodeX).getLeft()) {
                nodeX.getParent().setLeft(nodeY);
            } else {
                nodeX.getParent().setRight(nodeY);
            }
        }
        if (nodeY != null) {
            nodeY.setParent(nodeX.getParent());
        }
    }

    public void leftRotate(RBNode<E> n) {
        RBNode<E> r = n.getRight();
        replaceNode(n, r);
        n.setRight(r.getLeft());
        if (r.getLeft() != null) {
            r.getLeft().setParent(n);
        }
        r.setLeft(n);
        n.setParent(r);
    }

    public void rightRotate(RBNode<E> n) {
        RBNode<E> l = n.getLeft();
        replaceNode(n, l);
        n.setLeft(l.getRight());
        if (l.getRight() != null) {
            l.getRight().setParent(n);
        }
        l.setRight(n);
        n.setParent(l);
    }
    /*/
     public void leftRotate(RBNode<E> x) {
     RBNode<E> y = x.getRight();
     // Turn y's left sub-tree into x's right sub-tree         
     x.setRight(y.getLeft());
     if(y.getLeft() != null) {
     y.getLeft().setParent(x);
     }
     // y's new parent was x's parent 
     y.setParent(x.getParent());
      
     // Set the parent to point to y instead of x 
     // First see whether we're at the root 
     if(x.getParent()==null) {
     root = y;
     } else {
     // set y on parent's left when x was left child otherwise vice versa
     if(x==(x.getParent().getLeft()))
     x.getParent().setLeft(y);
     else
     x.getParent().setRight(y);            
     }
        
     // Finally, put x on y's left 
     y.setLeft(x);
     x.setParent(y);
     }
     
     public void rightRotate(RBNode<E> y) {
     RBNode<E> x = y.getLeft();
     // turn x's right sub tree into y's left 
     y.setLeft(x.getRight());
     if (x.getRight() != null) {
     x.getRight().setParent(y);
     }
     // parent change
     x.setParent(y.getParent());

     // parent node's child change
     // check if root 
     if (y.getParent() == null) {
     root = x;
     } else {
     if (y == (y.getParent().getLeft())) {
     y.getParent().setLeft(x);
     } else {
     y.getParent().setRight(x);
     }
     }

     //Finally, put y on x's right
     x.setRight(y);
     y.setParent(x);
     }
     /**/

    private void insert_case1(RBNode<E> node) {
        // case 1: node is root    
        if (node.getParent() == null) {
            node.setColour(RBNode.Color.BLACK);
        } else {
            insert_case2(node);
        }
    }

    private void insert_case2(RBNode<E> node) {
        // case 2 : parent is black then nothing
        
        if (parent(node).getColour() == RBNode.Color.RED) {
            insert_case3(node);
        }
    }

    private void insert_case3(RBNode<E> node) {
        // case 3 : the uncle node is red 
        RBNode uncle = uncle(node);
        if (uncle != null && uncle.getColour() == RBNode.Color.RED) {
            node.getParent().setColour(RBNode.Color.BLACK);
            uncle(node).setColour(RBNode.Color.BLACK);
            grandParent(node).setColour(RBNode.Color.RED);
            insert_case1(grandParent(node));
        } else {
            insert_case4(node);
        }
    }

    private void insert_case4(RBNode<E> node) {
        if (node == parent(node).getRight() && parent(node) == grandParent(node).getLeft()) {
            leftRotate(node.getParent());
            node = node.getLeft();
        } else if (node == parent(node).getLeft() && parent(node) == grandParent(node).getRight()) {
            rightRotate(node.getParent());
            node = node.getRight();
        }
        insert_case5(node);
    }

    private void insert_case5(RBNode<E> node) {
        RBNode g = grandParent(node);

        parent(node).setColour(RBNode.Color.BLACK);
        g.setColour(RBNode.Color.RED);

        if (node == parent(node).getLeft()) {
            rightRotate(g);
        } else {
            leftRotate(g);
        }
    }

    @Override
    public boolean add(E value) {
        RBNode node = new RBNode(value);

        if (root == null) {
            root = node;
        } else if (root.add(node) == null) {
            return false;
        }
        insert_case1(node);

        verifyProperties();
        return true;
    }
}
