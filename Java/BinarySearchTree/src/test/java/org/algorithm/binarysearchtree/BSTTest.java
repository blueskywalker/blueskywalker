/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarysearchtree;

import org.algorithm.binarytree.bst.BST;
import org.algorithm.binarytree.BTJob;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author blueskywalker
 */
public class BSTTest {
    
    public BSTTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    int [] input = new int[] { 10,5,15,12,2 };
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
       
    }

    private void setInput(BST<Integer> bst ) {
        
        for(int a : input) {
            bst.add(a);
        }
    }

    /**
     * Test of add method, of class BST.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        BST<Integer> bst = new BST<Integer>();        
        setInput(bst);
                
        boolean result = bst.add(20);
        assertEquals(true, result);
        result = bst.add(20);
        assertEquals(false, result);
    }

    /**
     * Test of search method, of class BST.
     */
    @Test
    public void testSearch() {
        System.out.println("search");
        BST<Integer> bst = new BST<Integer>();        
        setInput(bst);
        
        Integer result = bst.saerch(15);
        System.out.println(result);
        assertEquals(15, result.intValue());
    }

    /**
     * Test of inOrderJob method, of class BST.
     */
    @Test
    public void testInOrderJob() {
        System.out.println("inOrderJob");
        BST<Integer> instance = new BST<Integer>();
        setInput(instance);
        instance.inOrderJob(new BTJob() {

            public void doIt(Object value) {
                System.out.println(value);
            }
        });
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of preOrderJob method, of class BST.
     */
    @Test
    public void testPreOrderJob() {
        System.out.println("preOrderJob");
        BST<Integer> instance = new BST<Integer>();
        setInput(instance);
        instance.preOrderJob(new BTJob() {

            public void doIt(Object value) {
                System.out.println(value);
            }
        });
  
    }

    /**
     * Test of postOrderJob method, of class BST.
     */
    @Test
    public void testPostOrderJob() {
        System.out.println("postOrderJob");
       BST<Integer> instance = new BST<Integer>();
        setInput(instance);
        instance.postOrderJob(new BTJob() {

            public void doIt(Object value) {
                System.out.println(value);
            }
        });
  
    }
}