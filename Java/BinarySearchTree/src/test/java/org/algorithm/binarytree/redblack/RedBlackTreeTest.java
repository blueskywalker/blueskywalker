/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.algorithm.binarytree.redblack;

import org.algorithm.binarytree.BTJob;
import org.algorithm.binarytree.Node;
import org.junit.Test;


/**
 *
 * @author blueskywalker
 */
public class RedBlackTreeTest {

    int [] input = new int[] { 10,5,15,12,2,20,4,8,11,14 };
    
    /**
     *
     */
    
    public RedBlackTreeTest() {
        
    }
    
    @Test
    public void testRedBlackTreeTest() {
        RedBlackTree<Integer> tree = new RedBlackTree<Integer>();
        
        for(Integer item: input) {
            tree.add(item);
            System.out.println("==");
            tree.preOrderJob(new BTJob() {

                public void doIt(Object value) {
                    System.out.println(value);
                }
            });
        }
        
        Integer fifteen = tree.saerch(15);
        
        if(fifteen!=null) {
            System.out.println(fifteen);
        }
        
    }

}