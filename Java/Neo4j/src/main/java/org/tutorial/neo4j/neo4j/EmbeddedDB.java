package org.tutorial.neo4j.neo4j;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

/**
 * Hello world!
 *
 */
public class EmbeddedDB {

    private static enum RelTypes implements RelationshipType {

        KNOWS
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {

        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running example before it's completed)
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    public static void main(String[] args) {
        String dbPath = "/Users/blueskywalker/tmp/neo4jDB";
        GraphDatabaseService graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(dbPath);

        registerShutdownHook(graphDB);

        Node firstNode;
        Node secondNode;
        Relationship relationship;
        Transaction tx = graphDB.beginTx();
        try {
            firstNode = graphDB.createNode();
            firstNode.setProperty("message", "Hello, ");
            secondNode = graphDB.createNode();
            secondNode.setProperty("message", "World!");

            relationship = firstNode.createRelationshipTo(secondNode, RelTypes.KNOWS);
            relationship.setProperty("message", "brave Neo4j ");

            // Updating operations go here
            tx.success();
        } finally {
            tx.finish();
        }

        System.out.print(firstNode.getProperty("message"));
        System.out.print(relationship.getProperty("message"));
        System.out.print(secondNode.getProperty("message"));
        System.out.println("");

        tx = graphDB.beginTx();
        try {
        // let's remove the data
        firstNode.getSingleRelationship(RelTypes.KNOWS, Direction.OUTGOING).delete();
        firstNode.delete();
        secondNode.delete();
            // Updating operations go here
            tx.success();
        } finally {
            tx.finish();
        }        
        graphDB.shutdown();
        
    }
}
