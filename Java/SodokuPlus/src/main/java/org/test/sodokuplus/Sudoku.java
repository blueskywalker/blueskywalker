/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.test.sodokuplus;

import java.util.BitSet;

/**
 *
 * @author blueskywalker
 */
public class Sudoku {

    private int[][] data;
    private int N;
    private int sqrtN;
    private BitSet testSet;

    public Sudoku(int[][] sodoku) throws IllegalArgumentException {
        data = sodoku;
        double dsqrtN = Math.sqrt(data.length);
        sqrtN = (int) dsqrtN;
        if (data == null
                || data.length == 0
                || data.length != data[0].length
                || dsqrtN != sqrtN) {
            throw new IllegalArgumentException("wrong input");
        }
        N = data.length;

        testSet = new BitSet(sqrtN + 1);

    }

    private void clearTest() {
        testSet.clear();
    }

    private boolean isOkay() {
        return testSet.cardinality() == N;
    }

    public boolean isValid() {

        // test row uniq
        for (int i = 0; i < N; i++) {
            clearTest();
            for (int j = 0; j < N; j++) {
                testSet.set(data[i][j]);
            }
            if (!isOkay()) {
                return false;
            }
        }
        // test column uniq
        for (int i = 0; i < N; i++) {
            clearTest();
            for (int j = 0; j < N; j++) {
                testSet.set(data[j][i]);
            }
            if (!isOkay()) {
                return false;
            }
        }

        // test region
        for (int i = 0; i < sqrtN; i++) {
            for (int j = 0; j < sqrtN; j++) {
                if (!testRegion(i * sqrtN, (i + 1) * sqrtN, j * sqrtN, (j + 1) * sqrtN)) {
                   // System.out.println("wrong");
                    return false;
                }
            }
        }

        return true;
    }

    private boolean testRegion(
            int rowStart, int rowEnd,
            int colStart, int colEnd) {
        //System.out.println("===");
        clearTest();
        for (int i = rowStart; i < rowEnd; i++) {
            for (int j = colStart; j < colEnd; j++) {
                //System.out.println(String.format("data[%02d][%02d]-%d", i, j, data[i][j]));
                testSet.set(data[i][j]);
            }
        }
        return isOkay();
    }
}
