/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.test.sodokuplus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author blueskywalker
 */
public class CsvReader {
 
    String file;
    ArrayList<String []> data;
    
    public CsvReader(String fileName) {
        file=fileName;
        data = new ArrayList<String []> ();
    }
    
    public void parse() throws IOException {
        
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while((line=br.readLine())!=null) {
            data.add(line.split(","));
        }
        br.close();
    }
    
    public int [][] toSquare() {
        if(data.isEmpty() || data.size() != data.get(0).length) {
            return null;
        }
        int [][] ret = new int [data.size()][data.size()];
        
        for(int i=0;i<ret.length;i++) {
            for(int j=0;j<ret[i].length;j++) {
                ret[i][j] = Integer.parseInt(data.get(i)[j]);
            }
        }
        
        return ret;
    }
}
