package org.test.sodokuplus;

/**
 * Hello world!
 *
 */
public class SudokuPlus {

    public static void main(String[] args) {
        try {
            if(args.length!=1) {
                System.out.println("SudokuPlus inputfile");
                System.exit(-1);
            }
            
            CsvReader reader = new CsvReader(args[0]);
            
            reader.parse();
            int [][] input = reader.toSquare();
            if(input==null) {
                System.out.println("Input Format Wrong");
                System.exit(-1);
            }
            
            Sudoku sodoku = new Sudoku(input);
            
            boolean valid = sodoku.isValid();
            
            if(valid)
                System.out.println(String.format("%s is valid",args[0]));
            else
                System.out.println(String.format("%s is invalid",args[0]));
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
    }
}
