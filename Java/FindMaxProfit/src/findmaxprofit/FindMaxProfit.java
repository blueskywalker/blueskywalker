/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package findmaxprofit;

/**
 *
 * @author blueskywalker
 */
public class FindMaxProfit {

    static int [] prices = {100, 113, 110, 85, 105, 102, 86, 63, 81, 101, 94, 106, 101, 79, 94, 90, 97};
    /**
     * @param args the command line arguments
     */
    
    static void printArray(int [] list) {
        for(int i=0;i<list.length;i++) {
            System.out.printf("%3d",list[i]);
            if(i==list.length-1)
                break;
            System.out.print(",");
        }
        System.out.println("");
    }
    // O(N^2)
    static void bruteForce() {
        int N = prices.length;
        int [][] table = new int[N][N];
        int maxX=-1,maxY = -1;
        int max=0;
        
        for(int i=0;i<N;i++) {
            for(int j=i;j<N;j++) {
                table[i][j] = prices[j]-prices[i];
                if (table[i][j]>max) {
                    maxX=i;
                    maxY=j;
                    max = table[i][j];
                }
            }
        }
        
        for(int i=0;i<N;i++)
            printArray(table[i]);
        
        System.out.printf("%d-%d:%d\n",max,maxX,maxY);
    }
    
    static void maxsubarray() {
        int [] changes = new int [ prices.length ];
        
        for(int i=1;i<prices.length;i++) {
            changes[i]=prices[i]-prices[i-1];
        }
        printArray(changes);
        int max_end_here=0,max_soFar=0;
        
//        for(int i : changes) {
//            max_end_here = Math.max(0, max_end_here+i);
//            max_soFar = Math.max(max_soFar, max_end_here);
//        }

        int max_buy=-1;
        int max_sell=-1;
        int start=0;
        for(int i=0;i<changes.length;i++) {
            max_end_here+=changes[i]; 
            if(0 > max_end_here)  {
                 max_end_here=0;
                 start=i;
             }
             if(max_soFar<max_end_here) {
                 max_soFar = max_end_here;
                 max_buy=start;
                 max_sell=i;
             }
        }
        System.out.printf("%d-%d:%d\n",max_soFar,max_buy,max_sell);
    }
    
    static void divide_and_conquor() {
        int [] changes = new int [ prices.length ];
        
        for(int i=1;i<prices.length;i++) {
            changes[i]=prices[i]-prices[i-1];
        }
        printArray(changes);
        
    }
    public static void main(String[] args) {
        printArray(prices);
        bruteForce();
        printArray(prices);
        maxsubarray();
    }
}
