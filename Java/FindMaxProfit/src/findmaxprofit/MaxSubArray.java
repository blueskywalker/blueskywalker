/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package findmaxprofit;

/**
 *
 * @author blueskywalker
 */
public class MaxSubArray {

    int [] array;
    int bottom;
    int top;
    int sum;

    public MaxSubArray(int[] input) {
        array = input;
    }

    public void linearMetho() {
        int max_end_here=0, max_soFar=0;
        
        for (int i = 0; i < array.length; i++) {
            max_end_here += array[i];
            if (0 > max_end_here) {
                max_end_here = 0;
                bottom = i;
            }
            if (max_soFar < max_end_here) {
                max_soFar = max_end_here;
                top = i;
            }
        }
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
    
}
