package randomnumber;

import java.util.Random;

/**
Design and implement a class to generate random numbers 
in an arbitrary probability distribution given by an array of integer weights, i.e. 
 for int[] w return a number, n, from 0 to w.length - 1 with probability w[n] / sum(w). 
 Using an existing random number generator with a uniform distribution is permitted. 

Example distribution: 
w = 1, 2, 3, 2, 1 

Example probabilities: 
w / sum = 1/9, 2/9, 1/3, 2/9, 1/9 

Example results: 
n = 0, 1, 2, 3, 4 

Documentation: 

Class java.util.Random 

public int nextInt(int n) 

Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value (exclusive), drawn from this random number generator's sequence. The general contract of nextInt is that one int value in the specified range is pseudorandomly generated and returned. All n possible int values are produced with (approximately) equal probability. 

Parameters: 
n - the bound on the random number to be returned. Must be positive. 
Returns: 
the next pseudorandom, uniformly distributed int value between 0 (inclusive) and n (exclusive) from this random number generator's sequence 
Throws: 
IllegalArgumentException - if n is not positivepackage randomnumber;

**/
/**
 *
 * @author jerry
 */
public class WRandom {
   
    private int [] input;
    private int sum;
    private int sqrtN;
    private float [] prob;
    private Random random;
    
    public static void main(String[] args) {
        int [] input = new int [] {1, 2, 3, 2, 1};
        
        WRandom random = new WRandom(input);
                    
        int []counts= new int[input.length];
        
        int N=100000;
        
        for(int i=0;i<N;i++) {
            counts[random.getNext()]++;
        }
        
        for(int i=0;i<counts.length;i++) {
            System.out.printf("[%d]%d/%d = %f\n",i,counts[i],N,(float)counts[i]/N);
        }
    }

    public WRandom(int []input) {
        this.input = input;
        
        sum=0;
        for(int i: input) {
            sum+=i;
        }
        
        double sqrt= Math.sqrt(sum);
        sqrtN = (int) sqrt;
        if (sqrt!= (double)sqrtN)
            throw new IllegalArgumentException();
        
        prob = new float[input.length];
        
        for(int i=0;i<prob.length;i++) {
            prob[i]=(float)input[i]/sum;
        }
        
        random = new Random();
    }
    
    public int getNext() {
        
        return random.nextInt(sqrtN)+random.nextInt(sqrtN);
        
    }

    public int getSum() {
        return sum;
    }

    public float[] getProb() {
        return prob;
    }

    
}
