package randomnumber;

import java.util.Random;

//I Got this Crazy Question on PHONE INTERVIEW AT GOOGLE: 
//
//Design and implement a class to generate random numbers in an arbitrary probability distribution given by an array of integer weights, i.e. for int[] w return a number, n, from 0 to w.length - 1 with probability w[n] / sum(w). Using an existing random number generator with a uniform distribution is permitted. 
//
//Example distribution: 
//w = 1, 2, 3, 2, 1 
//
//Example probabilities: 
//w / sum = 1/9, 2/9, 1/3, 2/9, 1/9 
//
//Example results: 
//n = 0, 1, 2, 3, 4 
//
//Documentation: 
//
//Class java.util.Random 
//
//public int nextInt(int n) 
//
//Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value (exclusive), drawn from this random number generator's sequence. The general contract of nextInt is that one int value in the specified range is pseudorandomly generated and returned. All n possible int values are produced with (approximately) equal probability. 
//
//Parameters: 
//n - the bound on the random number to be returned. Must be positive. 
//Returns: 
//the next pseudorandom, uniformly distributed int value between 0 (inclusive) and n (exclusive) from this random number generator's sequence 
//Throws: 
//IllegalArgumentException - if n is not positive

public class WeightRandom {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//1/9, 2/9, 1/3, 2/9, 1/9 
		float [] distro = {1.0f/9, 2.0f/9, 1.0f/3, 2.0f/9, 1.0f/9};
		int [] values = {0,1,2,3,4};
		WeightRandom gen = new WeightRandom(values, distro);
		
		int [] valuesCount = new int[5];
		for(int i = 0; i < 100000; i++)
		{
			valuesCount[gen.next()]++;
		}
		for(int i = 0; i < 5; i++)
		{
			System.out.println((float)valuesCount[i]/100000 + ", " + distro[i]);
		}
	}
	
	float [] distribution;
	int [] values;
	private Random random;
	
	public WeightRandom(int [] values, float [] distribution)
	{
		this.distribution = distribution;
		this.values = values;
		this.random = new Random();
	}
	
	public int next()
	{
		float prob = random.nextFloat();
		float probAt = distribution[0];
		int valAt = 0;
		while(probAt < prob)
		{
			probAt += distribution[valAt+1];
			valAt++;
		}
		return values[valAt];
	}
}
