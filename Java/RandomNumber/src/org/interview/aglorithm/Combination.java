/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.interview.aglorithm;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author jerry
 */

/*
 *  half dollars
 *  quaters
 *  dimes
 *  nickels
 *  pennys
 * 
 *  equals 1 dollar
 */
public class Combination {
    
    
    public static enum Coin { half(50),quater(25),dime(10),nickel(5),penny(1); 
            private final int value;
            
            Coin(int value) {
                this.value = value;
            }

        public int getValue() {
            return value;
        }
            
    };
    
    HashMap<Coin,Integer> combinations;
    int sum;
    
    Combination(int sum) {
        this.sum = sum;
        combinations = new HashMap<>();
        Set<Coin> set = EnumSet.allOf(Coin.class);
        Iterator<Coin> iter = set.iterator();
        
        while(iter.hasNext()) {
            combinations.put(iter.next(), 0);
        }
    }
    
    private int subTotal(ArrayList<Coin> kinds, int depth) {
        int sub=0;
        for(int i=0;i<depth;i++) {
            sub+=combinations.get(kinds.get(i))*kinds.get(i).getValue();
        }
        return sub;
    }
    
    private void printCombination() {
        for(Coin c: combinations.keySet()) {
            System.out.printf("[%s:%d]",c.name(),combinations.get(c));
        }
        System.out.println();
    }
    
    public void getCombinations(ArrayList<Coin> kinds, int depth) {
        
        if(kinds.size()==depth)
            return;
        
        int count=0;
        int subtotal = subTotal(kinds,depth);
        
        while(subtotal<sum) {
            combinations.put(kinds.get(depth), count++);
            getCombinations(kinds, depth+1);
            subtotal+=kinds.get(depth).getValue();
        }
        if(subtotal==sum) {
            combinations.put(kinds.get(depth), count);
            printCombination();
        }
        combinations.put(kinds.get(depth), 0);
    }
    
    public static void main(String[] args) {
        int sum = 100;
        ArrayList<Coin> kinds = new ArrayList<>();
        Set<Coin> set = EnumSet.allOf(Coin.class);
        Iterator<Coin> iter = set.iterator();
        
        while(iter.hasNext()) {            
            kinds.add(iter.next());
        }
        
        Combination com = new Combination(sum);
        
        com.getCombinations(kinds, 0);
    }
}
