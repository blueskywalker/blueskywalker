/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.interview.aglorithm;

/**
 *
 * @author blueskywalker
 */

/*
 *  N dices which have from 1 to A faces
 *  the number of ways to make sum S
 * 
 */

/*
 *   ex N=3
 *   given sum 4 
 *    
 *      
 */
public class DiceSum {
   
    public static int possible(int s,int n ,int max) {

        int methods=0;
        
        if(s<1) return 0;
        
        if(n==1) return 1;
       
        
        for(int i=1;i<=max;i++) {
            methods+= possible(s-i,n-1,max);
        }
        return methods;
    }
    
    public static void main(String[] args) {
        int N=3;
        int sum=10;
        int A=6;
        
        
        System.out.println(possible(sum, N, A));
        
    }
}
