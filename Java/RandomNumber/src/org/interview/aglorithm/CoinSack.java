/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.interview.aglorithm;

/**
 *
 * @author jerry
 */
public class CoinSack {
 
    private static int subtotal(int [] coins,int [] sack,int depth) {
        int subtotal=0;
        for(int i=0;i<=depth;i++) {
            subtotal+= coins[i] * sack[i];
        }
        return subtotal;
    }
    private static void printSack(int[] coins, int [] sack) {
        for(int i=0;i< coins.length;i++) {
            System.out.printf("[%d:%d]",coins[i],sack[i]);
        }
        System.out.println();
    }
    
    public static void getSack(int sum,int [] coins, int [] sack,int depth) {
    
        int subtotal= 0;

        if(coins.length<=depth)
            return;
        
        while(subtotal<sum) {
            getSack(sum,coins,sack,depth+1);
            sack[depth]++;
            subtotal = subtotal(coins,sack,depth);
        }
        
        if(subtotal==sum) {
            printSack(coins,sack);
        }
        sack[depth]=0;
    }
    
    public static void main(String[] args) {
        int [] coins = new int [] { 50,25,10,5,1};
        int [] sack = new int[] { 0,0,0,0,0};
        int sum = 100;
        
        getSack(sum,coins,sack,0);
    }
}
