/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanoi;

/**
 *
 * @author jerry
 */
public class Hanoi {

    private Stick [] sticks;
    private int totalDisks;
    
    public Hanoi(int n) {
        totalDisks = n;
        sticks = new Stick[3];
        
        for(int i=0;i<sticks.length;i++) {
            sticks[i] = new Stick();
        }
        
        for(int i=n;i>0;i--) {
            sticks[0].add(new Disk(i));
        }
    }
    
    public  void play() {
        move(totalDisks,0,1,2);
    }
    
    public Stick getStick(int n) {
        if(n>=0 && n<sticks.length) {
            return sticks[n];
        }
        return null;
    }
    
    private void move(int N,int from, int tmp,int to) {
        
        if(N==1) {
            Disk disk = sticks[to].push(sticks[from].pop());
            if(disk==null) {
                System.out.println("wrong size");
            } else {
                System.out.printf("move[%d]{%d->%d}\n",disk.getRadius(),from,to);
            }
            return;
            
        }
        move(N-1,from,to,tmp);
        move(1,from,tmp,to);
        move(N-1,tmp,from,to);
        
    }
    
    public static void main(String[] args) {
        Hanoi hanoi = new Hanoi(15);
        
        hanoi.play();
    
        System.out.println(hanoi.getStick(2));
        
    }
}
