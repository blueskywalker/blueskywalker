/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanoi;

import java.util.Iterator;
import java.util.Stack;

/**
 *
 * @author jerry
 */
public class Stick extends Stack<Disk> {

    @Override
    public Disk push(Disk item) {
        if (!empty() && peek().compareTo(item)<= 0)
            return null;
        
        return super.push(item); 
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        Iterator<Disk> iter = iterator();
        
        while(iter.hasNext()) {
            sb.append(iter.next());
            if(iter.hasNext()) {
                sb.append(',');
            }
        }
        
        return sb.toString();
    }
    
    
}
