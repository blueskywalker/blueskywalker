/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanoi;

/**
 *
 * @author jerry
 */
public class Disk implements Comparable<Disk>{

    private int radius;

    public Disk(int radius) {
        this.radius = radius;
    }

    
    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public int compareTo(Disk o) {
        return this.radius-o.radius;
    }

    @Override
    public String toString() {
        return "Disk{" + radius + '}';
    }

    
}
