/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package permutationstring;

import java.util.ArrayList;

/**
 *
 * @author blueskywalker
 */
public class Permute {

    char [] input;
    
    ArrayList<String> output;
    
    public Permute(String input) {
        this.input = input.toCharArray();
        output = new ArrayList<String>();
    }
    
    /*
     * permutation
     * P(abcd) = a * P(bcd) | b * P(acd) | c * P(abd) | d * P(abc)
     * P(abc) = a * P(bc) | b * P(ac) | c * P(ab)
     * P(ab) = a * P(b) | b * P(a)
     * P(a) = a
     * 
     */
    public void swap(int x,int y) {
        char tmp=input[x];
        input[x] = input[y];
        input[y]=tmp;
    }
    
    public void permute(int n) {
        if(input.length == n) {
            output.add(new String(input));
            return;
        }
        for(int i=n;i<input.length;i++) {
            swap(n,i);
            permute(n+1);
            swap(n,i);
        }
    }
    
    public ArrayList<String> getOutput() {
        return output;
    }
}
