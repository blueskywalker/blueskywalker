/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suffixtree;

import java.util.Set;

/**
 *
 * @author blueskywalker
 */
public class Node {
    private int start;
    private Edges edges;

    public Node(int start) {
        edges = new Edges();
        this.start=start;
    }

    public Set<Element> getEdges() {
        return edges;
    }

    public void setEdges(Edges edges) {
        this.edges = edges;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }




    int startsWith(String s1,String s2) {
        int length = (s1.length()<s2.length())?s1.length():s2.length();
        
        int i=0;
        for(;i<length;i++) {
            if(s1.charAt(i)!=s2.charAt(i))
                break;
        }
        if(i>0)
            return i;
        
        return -1;
    }
    
    void push(String str,int starts) {
        for(Element e : edges) {
            int offset = startsWith(str, e.getValue());
            if(offset>0) {
                String rest = e.getValue().substring(offset);
                Node node = e.getNode();
                edges.remove(e);
                e = new Element(str.substring(0, offset),node.getStart());
                edges.add(e);
                e.getNode().push(str.substring(offset),starts+offset);
                e.getNode().push(rest,node.getStart()+offset);
                return;
            }
        }
        edges.add(new Element(str,starts));
    }

    @Override
    public String toString() {
        return "Node{" + "start=" + start + ", edges=" + edges + '}';
    }



    
}
