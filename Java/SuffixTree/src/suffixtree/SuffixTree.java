/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suffixtree;

/**
 *
 * @author blueskywalker
 */
public class SuffixTree {

    private Node root;

    public SuffixTree() {
        root = new Node(0);
        
    }

    public Node getRoot() {
        return root;
    }
 
    public void printNode() {
        printNode(root);
    }
    public void printNode(Node node) {
        System.out.println(node);
        
        for(Element e: node.getEdges()) {
            printNode(e.getNode());
        }
    }
    
    public void push(String str,int start) {
        root.push(str,start);
    }
}
