/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suffixtree;

/**
 *
 * @author blueskywalker
 */
public class Element implements Comparable<Element>{
    public static final String EMPTY="";
    String value;
    Node node;

    Element(String str,int start) {
        value = str;
        node = new Node(start);
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Override
    public int compareTo(Element o) {
        return value.compareTo(o.value);
    }

    @Override
    public String toString() {
        return "Element{" + "value=" + value + '}';
    }



    
}
