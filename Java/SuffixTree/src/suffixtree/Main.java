/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suffixtree;

/**
 *
 * @author blueskywalker
 */
public class Main {

        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String test = "papua";
       // test = "mississippi";
        
        buildNaive(test);
        
    }
    
    //
    // O(N^2)
    // papua
    // 
    // a
    // ua
    // pua
    // apua
    // papua
    public static void buildNaive(String str) {
        
        String [] subs = new String[str.length()+1];
        
        SuffixTree tree = new SuffixTree();
        
        int i=0;
        for(;i<str.length();i++) {
            subs[i] = str.substring(i);
        }
        subs[i]= "";
        
        for(i=str.length();i>-1;i--) {
            tree.push(subs[i],i);
        }
        
        tree.printNode();
    }
}
