/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinetree.graph;

import java.io.Serializable;
import java.util.HashSet;

/**
 *
 * @author blueskywalker
 */
public class GraphNode implements Serializable {

    private String id;
    private String name;
    private String email;
    private HashSet<String>  friends;

    static final long serialVersionUID = -1125123666604354392L;
    
    public GraphNode() {
        friends = new HashSet<String>();
    }

    public GraphNode(String id, String name) {
        this.id = id;
        this.name = name;
        friends = new HashSet<String>();
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashSet<String> getFriends() {
        return friends;
    }

    public void setFriends(HashSet<String> friends) {
        this.friends = friends;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "GraphNode { \n" + "\tid=" + id + "\n\t name=" + name + "\n\t email=" + email 
                + "\n\t friends=" + friends + '}';
    }

}
