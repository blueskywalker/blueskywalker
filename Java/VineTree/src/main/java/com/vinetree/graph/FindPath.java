/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinetree.graph;

import java.util.ArrayList;

/**
 *
 * @author blueskywalker
 */
public class FindPath {
    private String id;
    private int depth;
    private ArrayList<String> back;

    public FindPath() {
    }

    public FindPath(String id, int depth, String back) {
        this.id = id;
        this.depth = depth;
        this.back = new ArrayList<String>();
        this.back.add(back);
    }

    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public ArrayList<String> getBack() {
        return back;
    }

    public void setBack(ArrayList<String> back) {
        this.back = back;
    }

    
    
}
