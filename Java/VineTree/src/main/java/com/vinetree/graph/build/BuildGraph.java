/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinetree.graph.build;

import com.vinetree.graph.Connect;
import com.vinetree.graph.FindPath;
import com.vinetree.graph.FriendSet;
import com.vinetree.graph.GraphNode;
import com.vinetree.graph.VineTreeGraph;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author blueskywalker
 */
public class BuildGraph {

    private VineTreeGraph graph;
    private String current;
    private int defaultListSize;

    public BuildGraph() {
        graph = new VineTreeGraph();
        defaultListSize = 10;
    }

    public void command() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            if (graph.isExist()) {
                graph.read();
                current = graph.getIdx().first();
            }

            String question = "Create[c]/Delete[d]/List[l]/Choose[h]/Save[s]/Find[f]/Quit[q]?";
            System.out.print(question);
            String line;


            while ((line = br.readLine()) != null) {
                String trim = line.trim();
                if (trim.equalsIgnoreCase("c") || trim.equalsIgnoreCase("create")) {
                    create(br);
                } else if (trim.equalsIgnoreCase("s") || trim.equalsIgnoreCase("save")) {
                    graph.save();
                } else if (trim.equalsIgnoreCase("l") || trim.equalsIgnoreCase("list")) {
                    list(br);
                } else if (trim.equalsIgnoreCase("h") || trim.equalsIgnoreCase("choose")) {
                    choose(br);
                } else if (trim.equalsIgnoreCase("f") || trim.equalsIgnoreCase("find")) {
                    find(br);
                } else if (trim.equalsIgnoreCase("d") || trim.equalsIgnoreCase("delete")) {
                    delete(br);
                } else if (trim.equalsIgnoreCase("q") || trim.equalsIgnoreCase("quit")) {
                    System.out.println("Good Bye");
                    System.exit(0);
                }
                System.out.print(question);
            }
        } catch (IOException ex) {
            Logger.getLogger(BuildGraph.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BuildGraph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        BuildGraph bg = new BuildGraph();
        bg.command();
    }

    private void create(BufferedReader br) throws IOException {
        GraphNode node = new GraphNode();

        System.out.println("CREATE ==========");
        System.out.print("id :");
        node.setId(br.readLine());
        System.out.print("name :");
        node.setName(br.readLine());
        System.out.print("email :");
        node.setEmail(br.readLine());

        if (graph.getIdx().contains(node.getId())) {
            System.out.print("Existing Id. update[u]/discard[d]");
            String cmd = br.readLine().trim();

            if (cmd.equalsIgnoreCase("u") || cmd.equalsIgnoreCase("update")) {
                delete(node.getId());
                graph.getGraph().put(node.getId(), node);
                current = node.getId();
            } else if (cmd.equalsIgnoreCase("d") || cmd.equalsIgnoreCase("discard")) {
                System.out.println("abandon input...");
            }
        } else {
            graph.getGraph().put(node.getId(), node);
            graph.getIdx().add(node.getId());
            current = node.getId();
        }
    }

    private void list(BufferedReader br) throws IOException {

        if (current == null) {
            return;
        }

        Iterator<String> it = graph.getIdx().tailSet(current, true).iterator();

        int count = 0;
        while (it.hasNext()) {
            current = it.next();
            System.out.printf("%s\t%s\n", current, graph.getGraph().get(current).getName());

            if (++count == defaultListSize) {
                System.out.print("break[b]");
                String cont = br.readLine().trim();
                if (cont.equalsIgnoreCase("b")) {
                    break;
                }
            }
        }
    }

    private void choose(BufferedReader br) throws IOException {
        System.out.print("Enter ID:");
        String id = br.readLine();
        if (!graph.getIdx().contains(id)) {
            System.out.println("There is no such a ID");
            return;
        }
        GraphNode node = graph.getGraph().get(id);
        current = node.getId();
        System.out.println(node);
        boolean backToMain = false;
        while (!backToMain) {
            System.out.print("Friend Add[a]/remove[d]/return[r]?");
            String cmd = br.readLine().trim();
            if (cmd.equalsIgnoreCase("a") || cmd.equalsIgnoreCase("add")) {
                System.out.print("Enter Id:");
                id = br.readLine();
                if (!graph.getIdx().contains(id)) {
                    System.out.println("there is no such a ID.");
                    continue;
                } else {
                    node.getFriends().add(id);
                    graph.getGraph().get(id).getFriends().add(node.getId());
                }
            } else if (cmd.equalsIgnoreCase("d") || cmd.equalsIgnoreCase("remove")) {
                System.out.println("Enter Id:");
                id = br.readLine();
                if (!graph.getIdx().contains(id) || !node.getFriends().contains(id)) {
                    System.out.println("there is no such a ID.");
                    continue;
                } else {
                    node.getFriends().remove(id);
                    graph.getGraph().get(id).getFriends().remove(node.getId());
                }
            } else if (cmd.equalsIgnoreCase("r") || cmd.equalsIgnoreCase("return")) {
                backToMain = true;
            }
        }
    }
    FriendSet friends;
    HashSet<String> finalFriends;

    private void find(BufferedReader br) throws IOException {
        System.out.print("Enter start ID:");
        String startId = br.readLine();
        System.out.print("Enter end ID:");
        String endId = br.readLine();
        if (!graph.getIdx().contains(startId)
                || !graph.getIdx().contains(endId)) {
            System.out.println("There is no such a ID.");
            return;
        }

        if (startId.equals(endId)) {
            System.out.println("start and end point are identical:" + endId);
            return;
        }
        friends = new FriendSet();
        finalFriends = new HashSet<String>();
        HashSet<String> target = new HashSet<String>();
        target.add(startId);

        find(startId, endId, target, 0);

        backtrace(startId, endId);

    }

    private void find(String start, String destination, HashSet<String> target, int depth) {
        HashSet<String> next = new HashSet<String>();

        if (finalFriends.size() > 0 || target.isEmpty()) {
            return;
        }

        for (String id : target) {
            for (String friend : graph.getGraph().get(id).getFriends()) {
                if (friend.equals(start)) {
                    continue;
                }

                FindPath path = friends.get(friend);
                if (path == null) {
                    if (friend.equals(destination)) {
                        finalFriends.add(id);
                    } else {
                        next.add(friend);
                        friends.put(friend, new FindPath(friend, depth + 1, id));
                    }
                } else {
                    if (path.getDepth() == (depth + 1)) {
                        path.getBack().add(id);
                    }
                }
            }
        }
        target = null;
        find(start, destination, next, depth + 1);
    }
    ArrayList<Connect> output;

    private ArrayList<Connect> backtrace1(String id, String start) {

        FindPath path = friends.get(id);

        ArrayList<Connect> ret= new ArrayList<Connect>();
        if (path == null) {
            ret.add(new Connect(id));
            
        } else {
            for (String back : path.getBack()) {
                for(Connect c: backtrace1(back, start)) {
                    c.add(id);
                    ret.add(c);
                }
            }
            
        }
        return ret;
    }

    private void backtrace(String start, String end) {

        for (String id : finalFriends) {
            for (Connect c : backtrace1(id, start)) {
                c.add(end);
                System.out.println(c);
            }
        }
    }

    private void deleteFriend(String id, HashSet<String> friends) {

        for (String friend : friends) {
            graph.getGraph().get(friend).getFriends().remove(id);
        }
    }

    private void delete(BufferedReader br) throws IOException {
        System.out.print("Enter Id:");
        String id = br.readLine();
        delete(id);
    }

    public void delete(String id) {
        if (graph.getIdx().contains(id)) {
            graph.getIdx().remove(id);
            deleteFriend(id, graph.getGraph().get(id).getFriends());
            graph.getGraph().remove(id);
        }
    }
}
