/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinetree.graph;

import java.util.HashMap;

/**
 *
 * @author blueskywalker
 */
public class FriendSet extends HashMap<String,FindPath> {
    private boolean done;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
    
}
