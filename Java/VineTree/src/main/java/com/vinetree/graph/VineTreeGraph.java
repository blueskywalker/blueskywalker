/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinetree.graph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.TreeSet;

/**
 *
 * @author blueskywalker
 */
public class VineTreeGraph  {
    public static final String filename = "graphDB.dat";
    public static final String indexname = "graphDB.idx";    
    private String    path;
    private HashMap<String, GraphNode> graph;
    private TreeSet<String> idx;

    
    public VineTreeGraph() {
        path = ".";
    }

    public boolean isExist() {
        File graph = new File(path+"/"+filename);
        File index = new File(path+"/"+indexname);
        
        if(graph.exists() && index.exists())
            return true;
        
        return false;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    

    public HashMap<String, GraphNode> getGraph() {
        return graph;
    }

    public void setGraph(HashMap<String, GraphNode> graph) {
        this.graph = graph;
    }

    public TreeSet<String> getIdx() {
        return idx;
    }

    public void setIdx(TreeSet<String> idx) {
        this.idx = idx;
    }

    public void read() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
        graph = (HashMap<String, GraphNode>) ois.readObject();
        ois.close();

        ois = new ObjectInputStream(new FileInputStream(indexname));
        idx = (TreeSet<String>) ois.readObject();
        ois.close();
    }
    
    public void save() throws FileNotFoundException, IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
        oos.writeObject(graph);
        oos.close();

        oos = new ObjectOutputStream(new FileOutputStream(indexname));
        oos.writeObject(idx);
        oos.close();
        System.out.printf("save data to %s\n", filename);
    }
    
}
